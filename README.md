# NIST Draft Regulation 800-204C Comment Notes and Timestamps

This is a research publication for NIST 800-204C that contains notes from the review video linked below.  This documentation is for timestamps, comments and insights.

#### NIST Draft Regulations Review - Commentary Video - https://youtube.com/watch?v=GOICyahDHHo

# Timestamps and Comments

## Introduction - Table of Contents and Scope:
#### --Timestamp-- General intro - https://youtu.be/GOICyahDHHo?t=791
#### --Timestamp-- Line 254 comment - https://youtu.be/GOICyahDHHo?t=1377
    -low code and no code discussion for team make-ups, the skill bar for certain development aspects is lower for folks with security knowledge wanting to build with low-code architectures
#### --Timestamp-- Value of community development resources for lowering labor costs, development time and receiving more support from others - https://youtu.be/GOICyahDHHo?t=1717

## Scope - Service Mesh Design Discussion:
#### --Timestamp-- Scope discussion - https://youtu.be/GOICyahDHHo?t=2275
#### --Timestamp-- Line 280 comment - https://youtu.be/GOICyahDHHo?t=2293
    -built in observability discussion
    -AI and machine learning can be used to accommodate monitoring and review processes to speed up code review and project validations performed by developers
    -why monitoring from day 1 saves time longterm, and increases development agility
#### --Timestamp-- Line 282 comment and stress test demo sample - https://youtu.be/GOICyahDHHo?t=2742
    -discussion around making stress testing more streamlined and easier to manage routinely
    -test application discussion, the value of using well known applications for testing
#### --Timestamp-- Line 284 commentary - https://youtu.be/GOICyahDHHo?t=3054
    -discussion around the value of agnostic development, and why it's relevant
    -policy enforcement, and why whitepaper construction is a good first step toward policy and process
#### --Timestamp-- Lines 289-301 reference platform discussion - https://youtu.be/GOICyahDHHo?t=3685
    -observability concept demo
    -discussion around operational AI to accommodate vulnerability management and chaos engineering (https://youtu.be/GOICyahDHHo?t=4014)

## DevSecOps Initiatives:
#### --Timestamp-- DoD initiative discussion - https://youtu.be/GOICyahDHHo?t=4264

## Container Orchestration and REsource Management Platform:
#### --Timestamp-- Using Docker and Kubernetes together for deployments, and Longhorn for state management and storage (Line 402) - https://youtu.be/GOICyahDHHo?t=4975
        -adding minIO to Longhorn makes it S3 capable as a storage and state management solution

## Control Plane:
#### --Timestamp-- Line 505 sidecar proxy comments - https://youtu.be/GOICyahDHHo?t=6115
    -after looking over sidecar proxy options, linkerd and traefik mesh can also be used for engineering sidecar proxies
    -Envoy is great, and those who don't know how to use envoy can explore other envoy based Cloud Native project to get the benefits that come from using Envoy directly
    -"VDI" (virtual desktop infrastructure) architectures can be used with API proxy portals like traefik as secure management portals to load balanced assets implemented with sidecare functionality (containerized webtops are able to provide this sort of functionality - here is a sample container of a webtop VDI resource: https://docs.linuxserver.io/images/docker-webtop)

## Organizational Preparedness:
#### --Timestamp-- Line 536 - https://youtu.be/GOICyahDHHo?t=6405
    -misconceptions about labor design, especially in regards to time management
    -why developers need to know how to work with "done-for-you" resources, deeper customization can come later
#### --Timestamp-- Line 546 comments - https://youtu.be/GOICyahDHHo?t=6791
    -before productizing, frontload time invested into infrastructure and Infrastructure as Code
    -9 contributing development members took 10 months to launch a single Cloud Native production node, we used a closed development space for the months leading to production
    -code policy to limit human error and reduce misconfigurations
    -implementing chaos engineering to accommodate organizational preparedness
#### --Timestamp-- Line 569 comments- https://youtu.be/GOICyahDHHo?t=7282
    -sucurity assurance via built in design features, such as zero trust (secure by design discussion)
    -why AI maturity planning or prebuilt maturity can factor in infrastructure decisions (line 573)
#### --Timestamp-- Line 579 comments and SAST/DAST demo - https://youtu.be/GOICyahDHHo?t=7703
    -sample demo of modular containerized SAST
#### --Timestamp-- Line 614 comments - https://youtu.be/GOICyahDHHo?t=8269
    -line 623 is where this discussion point ends
#### --Timestamp-- Line 628 comments - https://youtu.be/GOICyahDHHo?t=8947
    -continuous integraton and continous delivery discussion
#### --Timestamp-- Line 680 comments around bilding agnostic CI/CD developer pipelines - https://youtu.be/GOICyahDHHo?t=9727
    -using GitLab with Docker Desktop for serverless engineering
    -it should be known that "Docker Desktop" (not all versions of Docker, only Docker Desktop) has QEMU emulation built directly into it, allowing the use and engineering of multi-architecture images with reasources like BuildX with ease
#### --Timestamp-- Line 726 comments - https://youtu.be/GOICyahDHHo?t=10143
    -Docker Desktop is a very flexible tool for code build, and works with ARM and amd64 hosts to allow multi-architecture access in a single developer space (QEMU works very well for both testing and development)
    -k3d Kubernetes makes it very easy to spin up Kubernetes clusters for various purposes and use cases (k3d Kubernetes inside of Docker Desktop produces a QEMU backend for Kubernetes for low effort to get developing - this is outstanding for agility)
    -all of this makes automation easier which saves time and ultimately significant amounts of money at the same time
    -automate the boring stuff first! (some things are harder to get talent to deliver consistently and reliably with, so give humans the fun jobs and give the machines the work no one wants)

## Requirements for Automation Tools in CI/CD Pipelines:
#### --Timestamp-- Line 769-781 comments - https://youtu.be/GOICyahDHHo?t=10936
    -enablement tools make a massive difference in development productivity and efficiency, especially with growing teams
    -is line 774 "become hamper" a typo?

## Implementing DevSecOps Primitives for the Reference Platform:
#### --Timestamp-- Line 809 comments - https://youtu.be/GOICyahDHHo?t=11674
    -code type discussion for agnostic development
    -Why Grafana, Prometheus and Loki are great tools for observability

##  CI/CD Pipeline for Infrastructure as Code:
#### --Timestamp-- Line 911 comments - https://youtu.be/GOICyahDHHo?t=13155
    -hybrid cloud construction using VM's with containers
    -BuildX and agnostic tools like Terraform
#### --Timestamp-- Line 963 comments - https://youtu.be/GOICyahDHHo?t=13375
    -policy discussion for defining policy in architecture decisions
    -Longhorn is a great tool for raft based cloud attached storage taht plays well with S3 enabled resources like minIO

## Benefits of DevSecOps Primitives to Application Security in the Service Mesh:
#### --Timestamp-- Line 1112 comments - https://youtu.be/GOICyahDHHo?t=14533
    -discussion on benefits
